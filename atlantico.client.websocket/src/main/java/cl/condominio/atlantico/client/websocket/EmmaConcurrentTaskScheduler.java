package cl.condominio.atlantico.client.websocket;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.util.ErrorHandler;
import org.springframework.util.concurrent.ListenableFuture;

public class EmmaConcurrentTaskScheduler extends ConcurrentTaskScheduler{


  public EmmaConcurrentTaskScheduler() {
    super();
  }

  @Override
  public ScheduledFuture<?> schedule(Runnable task, Instant startTime)
  {
    System.out.println("schedule");
    return super.schedule(task, startTime);
  }

  @Override
  public ScheduledFuture<?> scheduleAtFixedRate(Runnable task, Instant startTime, Duration period)
  {
    System.out.println("schedule");
    return super.scheduleAtFixedRate(task, startTime, period);
  }

  @Override
  public ScheduledFuture<?> scheduleAtFixedRate(Runnable task, Duration period)
  {
    System.out.println("schedule");
    return super.scheduleAtFixedRate(task, period);
  }

  @Override
  public ScheduledFuture<?> scheduleWithFixedDelay(Runnable task, Instant startTime, Duration delay)
  {
    System.out.println("schedule");
    return super.scheduleWithFixedDelay(task, startTime, delay);
  }

  @Override
  public ScheduledFuture<?> scheduleWithFixedDelay(Runnable task, Duration delay)
  {
    System.out.println("schedule");
    return super.scheduleWithFixedDelay(task, delay);
  }

  @Override
  public boolean prefersShortLivedTasks()
  {
    System.out.println("schedule");
    return super.prefersShortLivedTasks();
  }

  @Override
  public void setScheduledExecutor(ScheduledExecutorService scheduledExecutor)
  {
    System.out.println("schedule");
    super.setScheduledExecutor(scheduledExecutor);
  }

  @Override
  public void setErrorHandler(ErrorHandler errorHandler)
  {
    System.out.println("schedule");
    super.setErrorHandler(errorHandler);
  }

  @Override
  public ScheduledFuture<?> schedule(Runnable task, Trigger trigger)
  {
    System.out.println("schedule");
    return super.schedule(task, trigger);
  }

  @Override
  public ScheduledFuture<?> schedule(Runnable task, Date startTime)
  {
    System.out.println("schedule");
    return super.schedule(task, startTime);
  }

  @Override
  public ScheduledFuture<?> scheduleAtFixedRate(Runnable task, Date startTime, long period)
  {
    System.out.println("schedule");
    return super.scheduleAtFixedRate(task, startTime, period);
  }

  @Override
  public ScheduledFuture<?> scheduleAtFixedRate(Runnable task, long period)
  {
    System.out.println("schedule");
    return super.scheduleAtFixedRate(task, period);
  }

  @Override
  public ScheduledFuture<?> scheduleWithFixedDelay(Runnable task, Date startTime, long delay)
  {
    System.out.println("schedule");
    return super.scheduleWithFixedDelay(task, startTime, delay);
  }

  @Override
  public ScheduledFuture<?> scheduleWithFixedDelay(Runnable task, long delay)
  {
    System.out.println("schedule");
    return super.scheduleWithFixedDelay(task, delay);
  }

  @Override
  public void execute(Runnable task)
  {
    System.out.println("schedule");
    super.execute(task);
  }

  @Override
  public void execute(Runnable task, long startTimeout)
  {
    System.out.println("schedule");
    super.execute(task, startTimeout);
  }

  @Override
  public Future<?> submit(Runnable task)
  {
    System.out.println("schedule");
    return super.submit(task);
  }

  @Override
  public <T> Future<T> submit(Callable<T> task)
  {
    System.out.println("schedule");
    return super.submit(task);
  }

  @Override
  public ListenableFuture<?> submitListenable(Runnable task)
  {
    System.out.println("schedule");
    return super.submitListenable(task);
  }

  @Override
  public <T> ListenableFuture<T> submitListenable(Callable<T> task)
  {
    System.out.println("schedule");
    return super.submitListenable(task);
  }

  @Override
  public int hashCode()
  {
    return super.hashCode();
  }

  @Override
  public boolean equals(Object obj)
  {
    return super.equals(obj);
  }

  @Override
  protected Object clone() throws CloneNotSupportedException
  {
    return super.clone();
  }

  @Override
  public String toString()
  {
    return super.toString();
  }



}
