package cl.condominio.atlantico.client.websocket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadLocalRandom;

import javax.xml.bind.DatatypeConverter;

import org.springframework.http.HttpHeaders;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

public class Application
{
  private static final String user="dcaceres";
  private static final String password="A_12345678";
  
  static public class MyStompSessionHandler extends StompSessionHandlerAdapter
  {
    private String userId;

    public MyStompSessionHandler(String userId)
    {
      this.userId = userId;
    }

    private void showHeaders(StompHeaders headers)
    {
      for (Map.Entry<String, List<String>> e : headers.entrySet())
      {
        System.err.print("  " + e.getKey() + ": ");
        boolean first = true;
        for (String v : e.getValue())
        {
          if (!first) System.err.print(", ");
          System.err.print(v);
          first = false;
        }
        System.err.println();
      }
    }

    private void sendJsonMessage(StompSession session)
    {
      ClientMessage msg = new ClientMessage(userId, "Connectado");
      session.send("/app/chat/java", msg);
    }

    private void subscribeTopic(String topic, StompSession session)
    {
      session.subscribe(topic, new StompFrameHandler()
      {

        @Override
        public Type getPayloadType(StompHeaders headers)
        {
          return Object.class;
        }

        @Override
        public void handleFrame(StompHeaders headers, Object payload)
        {
          System.err.println(payload.toString());
        }
      });
    }

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders)
    {
      System.err.println("Connected to server! Headers:");
      showHeaders(connectedHeaders);

      subscribeTopic("CHAT", session);
      sendJsonMessage(session);
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload)
    {
      // TODO Auto-generated method stub
      super.handleFrame(headers, payload);
    }

    @Override
    public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload,
        Throwable exception)
    {
      exception.printStackTrace();
      super.handleException(session, command, headers, payload, exception);
    }

    @Override
    public void handleTransportError(StompSession session, Throwable exception)
    {
      exception.printStackTrace();
      super.handleTransportError(session, exception);
    }


  }

  public static void main(String[] args) throws InterruptedException, ExecutionException
  {
    WebSocketClient simpleWebSocketClient = new StandardWebSocketClient();

    WebSocketStompClient stompClient = new WebSocketStompClient(simpleWebSocketClient);
    stompClient.setMessageConverter(new MappingJackson2MessageConverter());

 String url = "ws://190.113.12.24:8080/CHAT";

   //String url = "ws://localhost:80/" + Predefined.END_POINT_CHAT.getValue();


    StompSessionHandler sessionHandler = new MyStompSessionHandler(user);
    StompSession session = connect(stompClient, url, sessionHandler);



    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    for (;;)
    {
      System.out.print(user + " >> ");
      System.out.flush();
      String line = null;
      try
      {
        line = in.readLine();
      }
      catch (IOException e)
      {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      if (line == null) break;
      if (line.length() == 0) continue;
      TextMessage msg = new TextMessage();
      msg.setIdDB( String.format("%d", ThreadLocalRandom.current().nextInt(1, 999) ));
      msg.setLatitud( 0.0 );
      msg.setLatitud( 0.0 );
      msg.setSesionOrigen("eosorio");
      msg.setSesionDestino("/topic");
      msg.setFecCreacionTexto( new Date() );
      msg.setTexto(line);
      session.send("/app/atlanticochat/chat", msg);
    }
  }

  private static StompSession connect(WebSocketStompClient stompClient, String url, StompSessionHandler sessionHandler)
      throws InterruptedException, ExecutionException
  {
    HttpHeaders header=new HttpHeaders();

    header.put("Authorization",Arrays.asList("Basic "+ DatatypeConverter.printBase64Binary((user+":"+password).toString().getBytes())));


    WebSocketHttpHeaders wsHeades=new WebSocketHttpHeaders(header);

    StompSession session = stompClient.connect(url,wsHeades, sessionHandler).get();
    return session;
  }
}
