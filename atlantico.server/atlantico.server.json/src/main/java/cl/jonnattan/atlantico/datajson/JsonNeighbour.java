package cl.jonnattan.atlantico.datajson;

import lombok.Data;

@Data
public class JsonNeighbour {
	private String imei;
	private String name;
	private String depto;
	private String torre;
	private String mail;
	private Boolean pet;

	public JsonNeighbour() {

	}

}
