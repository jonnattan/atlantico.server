package cl.jonnattan.atlantico.datajson;

import lombok.Data;

@Data
public class JsonOpenDoor {
	private String imei;
	private String iddoor;
	private String door;

	public JsonOpenDoor() {

	}
}
