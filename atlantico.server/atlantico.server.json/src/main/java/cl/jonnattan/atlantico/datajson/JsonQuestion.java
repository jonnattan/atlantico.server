package cl.jonnattan.atlantico.datajson;

import lombok.Data;

@Data
public class JsonQuestion {
	private String nombre;
	private String depto;
	private String torre;

	public JsonQuestion() {
		//
	}
}
