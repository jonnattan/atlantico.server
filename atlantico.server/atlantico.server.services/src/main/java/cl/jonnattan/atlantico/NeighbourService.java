package cl.jonnattan.atlantico;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.jonnattan.atlantico.daos.IDaoNeighbour;
import cl.jonnattan.atlantico.datajson.JsonNeighbour;
import cl.jonnattan.atlantico.datajson.JsonOpenDoor;
import cl.jonnattan.atlantico.datajson.JsonQuestion;
import cl.jonnattan.atlantico.interfaces.IConvert;
import cl.jonnattan.atlantico.interfaces.IUDPComm;

@Service
public class NeighbourService {
	private final static Logger logger = Logger.getLogger(NeighbourService.class.getName());

	@Autowired
	EntityManager em;
	@Autowired
	IDaoNeighbour daoNeighbour;
	@Autowired
	IUDPComm comm;
	@Autowired
	IConvert conversor;

	@PostConstruct
	public void initialize() {
		// Se ejecuta despues de traer todos los atributos y justo al moemnto de
		// construccion
	}

	@PreDestroy
	public void destroy() {
		// cuando se destruye se ejecuta...
	}

	@Transactional
	public Boolean create(final JsonNeighbour user) {
		Neighbour cop = conversor.convert(user);
		if (cop != null)
			daoNeighbour.save(cop);
		return (cop != null);
	}

	@Transactional(readOnly = true)
	public JsonQuestion validateImei(String imei) {
		JsonQuestion ret = null;
		Optional<Neighbour> result = daoNeighbour.findById(imei);
		if (result.isPresent()) {
			ret = conversor.convert(result.get());
		} else
			logger.info("No encontro nada... retorna NULL");
		return ret;
	}

	@Transactional(readOnly = true)
	public List<JsonNeighbour> getNeighbours() {
		List<JsonNeighbour> list = new ArrayList<JsonNeighbour>();
		Iterable<Neighbour> result = daoNeighbour.findAll();
		if (result != null) {
			result.forEach((nei) -> {
				JsonNeighbour dato = conversor.convertir(nei);
				list.add(dato);
			});
		} else
			logger.info("No encontro nada... retorna NULL");
		return list;
	}

	public boolean openDoor(final JsonOpenDoor data) {
		boolean success = false;
		Optional<Neighbour> result = daoNeighbour.findById(data.getImei());
		if (result.isPresent()) {
			Neighbour neighbour = result.get();
			// abre la puerta
			String id = data.getIddoor();
			if (comm.write(id.getBytes())) {
				JsonQuestion ret = conversor.convert(neighbour);
				logger.info("Solicita abrir la puerta: " + ret);
				success = true;
			}

		} else
			logger.info("No encontro nada... retorna NULL");
		return success;
	}
}
