package cl.jonnattan.atlantico.interfaces;

public interface IUDPComm
{
  public boolean write( final byte[] aData );
}
