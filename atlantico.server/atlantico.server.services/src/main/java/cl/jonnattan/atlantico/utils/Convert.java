package cl.jonnattan.atlantico.utils;

import cl.jonnattan.atlantico.Neighbour;
import cl.jonnattan.atlantico.datajson.JsonNeighbour;
import cl.jonnattan.atlantico.datajson.JsonQuestion;
import cl.jonnattan.atlantico.interfaces.IConvert;

public class Convert implements IConvert
{
  @Override
  public JsonQuestion convert( final Neighbour neighbour )
  {
    if(neighbour ==null) return null;
    JsonQuestion dao = new JsonQuestion();
    dao.setNombre( neighbour.getName() );
    dao.setDepto(neighbour.getDepto());
    dao.setTorre( neighbour.getTorre() );  
    return dao;
  }
  
  @Override
  public Neighbour convert( final JsonNeighbour jsonObj )
  {
    if(jsonObj ==null) return null;
    Neighbour neighbour = new Neighbour();
    neighbour.setImei(jsonObj.getImei());
    neighbour.setName(jsonObj.getName());
    neighbour.setDepto( jsonObj.getDepto() );
    neighbour.setTorre(jsonObj.getTorre() );
    neighbour.setMail( jsonObj.getMail() );
    neighbour.setPet( jsonObj.getPet() );
    return neighbour;
  }

  @Override
  public JsonNeighbour convertir(final Neighbour aValue)
  {
    JsonNeighbour dato = new JsonNeighbour();
    
    dato.setImei( aValue.getImei() );
    dato.setName( aValue.getName() );
    dato.setMail( aValue.getMail() );
    dato.setTorre( aValue.getTorre() );
    dato.setDepto( aValue.getDepto() );
    dato.setPet( aValue.getPet() );
    return dato;
  }
  
}
