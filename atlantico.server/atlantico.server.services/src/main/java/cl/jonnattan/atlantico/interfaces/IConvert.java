package cl.jonnattan.atlantico.interfaces;

import cl.jonnattan.atlantico.Neighbour;
import cl.jonnattan.atlantico.datajson.JsonNeighbour;
import cl.jonnattan.atlantico.datajson.JsonQuestion;

public interface IConvert {
	public JsonQuestion convert( final Neighbour neighbour );
	public Neighbour convert( final JsonNeighbour jsonObj );
	public JsonNeighbour convertir(final Neighbour aValue);
	
}
