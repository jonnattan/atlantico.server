package cl.jonnattan.atlantico;

import java.util.List;
import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cl.jonnattan.atlantico.datajson.JsonNeighbour;
import cl.jonnattan.atlantico.datajson.JsonOpenDoor;
import cl.jonnattan.atlantico.datajson.JsonQuestion;

// @RequestScope // con esto no sera sounu 1 instancia del componente, sino que se crea y destruye 
// @SessionScope // para sesiones (el alcance es mientras dura la sesion) termian cuando se cierra el navegador o por timeout. no aplica destroy
// @ApplicationScope // Es muy parecido al singleton. 

@RestController
@RequestMapping("/")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class NeighbourController
{
  private final static Logger logger = Logger.getLogger(NeighbourController.class.getName());

  @Autowired
  private NeighbourService neiService;

  @ResponseBody
  @RequestMapping("/")
  public String home()
  {
    return "ATLANTICO";
  }

  @RequestMapping(value = "/validate/{imei}", method = RequestMethod.GET)
  public ResponseEntity<JsonQuestion> validate(@PathVariable String imei)
  {
    logger.info("Se pregunta por IMEI: " + imei);
    JsonQuestion neig = neiService.validateImei(imei);
    HttpStatus   resp = neig != null ? HttpStatus.OK : HttpStatus.CONFLICT;
    return new ResponseEntity<JsonQuestion>(neig, resp);
  }

  @RequestMapping(value = "/save", method = RequestMethod.POST)
  public ResponseEntity<Boolean> create(@Valid @RequestBody JsonNeighbour user)
  {
    Boolean success = neiService.create(user);
    return new ResponseEntity<Boolean>(success, HttpStatus.CREATED);
  }

  @RequestMapping(value = "/open", method = RequestMethod.POST)
  public ResponseEntity<Boolean> openDoor(@Valid @RequestBody JsonOpenDoor data)
  {
    Boolean success = neiService.openDoor(data);
    return new ResponseEntity<Boolean>(success, success ? HttpStatus.OK : HttpStatus.CONFLICT);
  }
  
  @GetMapping("/neighbours")
  public ResponseEntity<List<JsonNeighbour>> neighbours( )
  {
    List<JsonNeighbour> lista = neiService.getNeighbours();
    return new ResponseEntity<List<JsonNeighbour>>(lista, HttpStatus.OK);
  }
  

}
