package cl.jonnattan.atlantico;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "neighbours")
@NamedQueries({
  @NamedQuery(name = "Neighbour.findByImei", query = "SELECT e FROM Neighbour e WHERE e.imei = ?1 "),
  @NamedQuery(name = "Neighbour.findAll", query = "SELECT c FROM Neighbour c")
})
public class Neighbour implements Serializable 
{
  private static final long serialVersionUID = 1L;

  @Id
  @Column(nullable = false, unique = true)
  private String imei;

  private String name;

  private String depto;

  private String torre;

  @Column(name = "pet")
  private Boolean pet;

  private String mail;

  @Column(name = "fecha_registro")
  @Temporal(TemporalType.DATE)
  @DateTimeFormat(pattern = "dd-MM-yyyy")
  private Date ingreso;

  public Neighbour()
  {
    super();
  }

  @PrePersist
  public void prePersist()
  {
    ingreso = new Date();
  }
  
  
  public String getImei()
  {
    return imei;
  }

  public void setImei(String imei)
  {
    this.imei = imei;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getDepto()
  {
    return depto;
  }

  public void setDepto(String depto)
  {
    this.depto = depto;
  }

  public String getTorre()
  {
    return torre;
  }

  public void setTorre(String torre)
  {
    this.torre = torre;
  }

  public String getMail()
  {
    return mail;
  }

  public void setMail(String mail)
  {
    this.mail = mail;
  }

  public Boolean getPet()
  {
    return pet;
  }

  public void setPet(boolean pet)
  {
    this.pet = pet;
  }



}
