package cl.jonnattan.atlantico.daos;

import org.springframework.data.repository.CrudRepository;

import cl.jonnattan.atlantico.Neighbour;

public interface IDaoNeighbour extends CrudRepository<Neighbour, String>
{
  // espectacular
}
